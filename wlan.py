import subprocess


def exssids() -> str:
    ssidss = "Newsderz"
    return ssidss


def wifis() -> str:
    wifi = str(subprocess.check_output("netsh wlan show networks"))
    return wifi


##########     Constructor Injection     ##########

class GetWifi:

    def __init__(self, cmd: str, exssid: str) -> None:
        self.cmd = cmd  # <-- dependency is injected
        self.exssid = exssid  # <-- dependency is injected

        ssids = str(subprocess.check_output(cmd))

        if exssid in ssids:
            print("ssid found (Constructor Injection)")
        else:
            print("ssid not found (Constructor Injection) Danger!!! you`re not at home !!!")


#########     Setter Injection     ##########

class SGetWifi:

    def __init__(self):

        self.exssid = None

        self.cmd = None

        self.ssids = None

    def set_exssid(self, exssid: str):

        self.exssid = exssid  # <-- dependency is injected

    def set_cmd(self, cmd: str):

        self.cmd = cmd  # <-- dependency is injected

    def validate_dependencies(self):

        if self.cmd is None:
            raise ValueError("cmd is not set")

        if self.exssid is None:
            raise ValueError("exssid is not set")

    def get_ssids(self):
        ssids = str(subprocess.check_output(self.cmd))
        return ssids

    def check(self):

        self.validate_dependencies()
        self.ssids = self.get_ssids()
        if self.exssid in self.ssids:
            print("ssid found (Setter Injection)")
        else:
            print("ssid not found (Constructor Injection) Danger!!! you`re not at home !!!")


#########     Method Injection     ##########

class MGetWifi:
    ssids = wifis()  # <-- dependency is injected
    exssid = exssids()  # <-- dependency is injected

    if exssid in ssids:
        print("ssid found (Method Injection)")
    else:
        print("ssid not found (Constructor Injection) Danger!!! you`re not at home !!!")
