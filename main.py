import wlan

if __name__ == "__main__":
    # Constructor Injection #

    get_ssids_cmd = "netsh wlan show networks"

    ssid = "Newsderz"

    wlan.GetWifi(cmd=get_ssids_cmd, exssid=ssid)

    # Setter Injection #

    mywifi = wlan.SGetWifi()

    mywifi.cmd = get_ssids_cmd

    mywifi.exssid = "Newsderz"

    mywifi.check()

    # Method Injection #

    wlan.MGetWifi
